<?php
include './shape/Shape.php';
include './shape/CircleShape.php';
include './shape/SquareShape.php';

include './builders/BuilderShape.php';
include './builders/BuilderCircle.php';
include './builders/BuilderSquare.php';

include './include/ShapeManager.php';

$arrShapes = [
    ['type' => 'circle', 'params' => ['color'=>'#278627']],
    ['type' => 'square', 'params' => ['color'=>'#0f3690']]
];

$mgr = new ShapeManager();

$mgr->setBuilderShape('circle', new BuilderCircle);
$mgr->setBuilderShape('square', new BuilderSquare);

$mgr->setShape($arrShapes);

$mgr->run();
