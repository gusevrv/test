<?php

/**
 * Created by PhpStorm.
 * User: h
 * Date: 19.03.2017
 * Time: 12:05
 */
class ShapeManager
{
    /**
     * Input array shapes
     * @var array
     */
    private $arrShapes = array();

    /**
     * Contains object type BuilderShape.
     *
     * @var array
     * @see BuilderShape
     */
    private $builders = array();

    /**
     * Set shapes, format [['type' => 'circle', 'params' => [...], ...].
     *
     * @param array $arrShapes
     */
    public function setShape(array $arrShapes)
    {
        $this->arrShapes = $arrShapes;
    }

    /**
     *  Run program.
     */
    public function run()
    {
        foreach ($this->arrShapes as $value) {
            $shape = $this->constructShape($value['type']);
            if ($shape) {
                if ($shape->setParameters($value['params']) === true) {
                    $shape->draw();
                }
            }
        }
    }

    /**
     * Set Builder shape.
     *
     * @param string $shapeName example 'circle'
     * @param BuilderShape $builder
     */
    public function setBuilderShape(string $shapeName, BuilderShape $builder)
    {
        $this->builders[$shapeName] = $builder;
    }

    /**
     * Construct shape form builder shapes.
     *
     * @param string $type example 'circle'
     * @return bool|Shape
     */
    private function constructShape(string $type)
    {
        // находим небоходимую фабрику для производстав фигуры
        $builder = $this->builders[$type] ?? null;
        if ($builder) {
            return $builder->crateShape();
        }
        return false;
    }
}