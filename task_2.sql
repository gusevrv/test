CREATE TABLE `authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `books_authors` (
  `id_book` int(11) NOT NULL,
  `id_authors` int(11) NOT NULL,
  FOREIGN KEY (id_book) REFERENCES books(id),
  FOREIGN KEY (id_authors) REFERENCES authors(id),
  CONSTRAINT UC_KEY UNIQUE (id_book,id_authors)
) ENGINE=InnoDB;