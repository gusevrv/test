<?php

/**
 * Created by PhpStorm.
 * User: h
 * Date: 19.03.2017
 * Time: 13:04
 */
abstract class BuilderShape
{
    protected $draw;

    public function setDraw(DrawStrategy $draw)
    {
        $this->draw = $draw;
    }
    abstract public function crateShape():Shape;
}