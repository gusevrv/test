<?php

/**
 * Created by PhpStorm.
 * User: h
 * Date: 19.03.2017
 * Time: 13:18
 */
abstract class Shape
{
    /**
     * Valid input parameters.
     * @see setParameters()
     * @var array
     */
    private $param = array();

    /**
     * Validating input parameters.
     * @param array $param
     * @return bool
     */
    abstract protected function validatorParameters(array $param):bool;

    /**
     * Render shape
     * @param array $param
     */
    abstract protected function render(array $param);

    /**
     * Drawing shape.
     * @return void
     */
    public function draw()
    {
        $param = $this->param;
        if ($param) {
           print $this->render($param);
        }
    }

    /**
     * @param array $param
     * @return bool
     */
    public final function setParameters(array $param):bool
    {
        if ($this->validatorParameters($param) === true) {
            $this->param = $param;
            return true;
        }
        return false;
    }
}