<?php

/**
 * Created by PhpStorm.
 * User: h
 * Date: 19.03.2017
 * Time: 13:21
 */
class CircleShape extends Shape
{
    /**
     * Validating input parameters
     * @param array $param
     * @return bool
     */
    protected function validatorParameters(array $param):bool
    {
        // TODO: необходима валидация обязательных параметров
        if (!empty($param['color'])) {
            return true;
        }
        return false;
    }

    /**
     * @param array $param
     */
    protected function render(array $param)
    {
        return '<div style="color:'.$param['color'].';">Круг</div>';
    }
}