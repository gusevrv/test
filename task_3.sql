SELECT main.`type`, main.`value`
FROM `data` as main
INNER JOIN
    (
    SELECT `type`, MAX(`date`) as MaxDate FROM `data` GROUP BY `type`
    ) gr_fnd_mx 
ON main.type = gr_fnd_mx.type 
AND main.date = gr_fnd_mx.MaxDate